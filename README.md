# TrekBike0809_AT

This repository contains the TC  for TrekBike


**Assignment TrekBikes_Ameet Thakkar [08.09.2019]**

**Implementation Steps**
1. Application in use (.apk) file from. Installed via ADB locally on the Galaxy S9 real device.

**Tools and Environments user**
1) Test script- Written in Java  
2) Java Appium Client- v7.1.0 installed  
3) JDK(1.8.X) installed  
4) IntelliJ version 2019.2 installed  
5) Maven v3.6.1 installed  

2) Steps to follow to Run the Script on Real Device
Local Script to Run on Real Device (DUT: Galaxy S9 , OS 9)  
**Pre-requiste**  
1) Ensure that your Android device is unlocked and not sleeping while connected via USB. Set the appropriate option in Settings or Developer Options  
2) Make sure your Android device is enabled for USB debugging. On many Android devices, you can verify whether USB debugging is enabled by visiting the Settings|Developer Options page  .
3) Install the USB driver for your Android device.  
a) Connect the Real device using the USB cable to the computer. Ensure that Android SDK is installed on the system and from the cmd prompt inside the Platform-tools folder type "adb devices".  
b) Open the local folder where Android SDK files has been saved 'Android SDK >> Platform-tools' eg: C:\android-sdk\platform-toolsAndroid Debug Bridge (adb) is a versatile command-line tool that lets you communicate with a device.  
c) Then in  command prompt type command: adb devices & press Enter. It will display all list of all the connected devices... The Connection state has 3 status, 1) Offline, 2) Device & 3) No device. So if the connection status is #2) Device, then the device UDID is displayed. Then Install the .apk (TrekBike) app on the device.
d) Open the Intellij Java Script is created by the name "TrekRD.java" that is based on TESTNG framework, which is executed by "testng.xml" runner  .
To execute or play this runner, a local appium setup( appium GUI server should be started ). Appium GUI server locally installed will be on host : 0.0.0.0 with port#4723. Start the Appium Server  .
e) Right click the testng.xml in intellij and select the "Run" Button for the test to execute and it will load the appium & device capabilities and run through the script, launch the application, get the text present on the screen and verify it and then finally close the app and also quit the driver.
A testNG report is shown on the console with Total Test Run and other status's as  per the script  .
The Test Run's successful and Script passes as per the Requirement OR Even Right Click the "TrekRD.java" and select "Run TrekRD" to execute the script.  

**Steps to Run the Script on Emulator.**  
a) Pre-Requisite- SDK (Software Development Kit) should be installed on the machine. ADB is packaged with Google's Android SDK (Software Development Kit). Steps to enable ADB from SDK Manager.  

Step 1) Open Android SDK folder    

Step 2) Double click on SDK Manager    

Step 3) From the list of all packages select Tools and mark the checkbox for  
Android SDK Tools and Android SDK Platform-tools. (for example Andriod 9 (API 28)  

**To create an Android emulator on your system**

1) Start the Android SDK Manager  

2) In the Android SDK Manager, select Tools | Manage AVDs.  
3) In the Android Virtual Device Manager, click the New button to create a new virtual device.  
4) Download the the Samsung Android Emulator skins for Samsung Galaxy S9 or other Galaxy S latest models- https://developer.samsung.com/galaxy/emulator-skin  
5) When downloads are done, extract the zip files in the path of your Android Studio installation that is like that : Android Studio > plugins > android > lib > device-art-resources  
6) Now, you can launch Android Studio and go to Tools > Android > AVD manager entry  
7) In the AVD Manager windows, click on “Create Virtual Device”  
8) Click “New Hardware Profile” on the Virtual Device Configuration section, Enter the Specifications for Galaxy S9 device  
9) In the Default skin section, pick the folder of zip file you have extracted previously in the device-art-resources folder. Select the system image for the  VD and complete the confirguration  
10) On the AVD Manager, you can see your new virtual device listed. To launch it, you have just to select if and click on the play icon  
11) After some seconds, may be minutes if your computer is slow, the Android Emulator skinned must appear on your screen.  
13)  Open cmd prompt inside the Platform-tools folder type "adb devices". The running ADB server can scan all connected emulator or device instances. Install the .apk (TrekBike) app on the device  .
14)  Open the Intellij Java Script is created by the name "TrekRD.java" that is based on TESTNG framework, which is executed by "testng.xml" runner.  
To execute or play this runner, a local appium setup( appium GUI server should be started ). Appium GUI server locally installed will be on host : 0.0.0.0 with port#4723. Start the Appium Server.  
15) Right click the testng.xml in intellij and select the "Run" Button for the test to execute and it will load the appium & device capabilities and run through the script, launch the application, get the text present on the screen and verify it and then finally close the app and also quit the driver.  
A testNG report is shown on the console with Total Test Run and other status's as  per the script.  
The Test Run's successful and Script passes as per the Requirement OR Even Right Click the "TrekRD.java" and select "Run TrekRD" to execute the script.  


Next steps:  
a)Created a Bitbucket Repository  

b)Setup and configure Jenkins locally on the system.  

c) Build the project & verify the result.  

