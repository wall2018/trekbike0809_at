/**This is simple program and it works - test passes here
It uses Real device connected to my laptop and Appium server is started Manually.
 Author: Ameet Thakkar*/

package testCases;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.concurrent.TimeUnit;


public class TrekRD {

	//Initialize the Appium Driver

	public static AndroidDriver<WebElement> driver;

	/**Before Test Annotation will run the method before any 'test' method.
	**This file is used for loading all the properties, setting up the Desired Capabilities
    **The annotated method will be run before any test method belonging to the classes inside the <test> tag is run**/

	@BeforeTest
	public void InitConfig() {

		try {

			// Define the Declare the desired Capabilities for the device in Test- Galaxy S9 Real device used here
			DesiredCapabilities cap = new DesiredCapabilities();

			cap.setCapability(MobileCapabilityType.DEVICE_NAME, "4b4549534c573398");
			cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
			cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
			cap.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
			//cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 20);
			cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "r_and_d_1.trekbikes.com.trekrd1");
			cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "r_and_d_1.trekbikes.com.trekrd1.MainActivity");

			driver = new AndroidDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		} catch (Exception exp) {
			System.out.println("Cause is: " + exp.getCause());
			System.out.println((" Message is : " + exp.getMessage()));
			exp.printStackTrace();

		}
	}

	// Actual Test Case script to a) Display the Text and b) verify if the text is "Believe in bikes"

	@Test(description = "Test Case 1: Verify the Text")
	public void verifyText() {

		// declare and initialize the variable to store the expected text.
		String expectedText = "Believe in bikes";
		//Find the Element with one of the Locator techniquies  By AndroidUIAutomator
		WebElement bodyText = driver.findElementByAndroidUIAutomator("new UiSelector().className(\"android.widget.TextView\")");
		System.out.println("Text is Displayed = " + bodyText.getText());
		try {
			// // compare the expected text of the page with the actual text of the page and print the result
			Assert.assertEquals(bodyText.getText(), expectedText);
			System.out.println("Verification Successful");
		} catch (NoSuchElementException e) {
			System.out.println("Text is not Displayed");
		}
	}

	    //The annotated method will be run after all the test methods belonging to the classes inside the <test> tag have run.
		@AfterTest
		public void tearDown () {
		 // It is a good practise to Close the application & quit the driver after the test is run. These are the last statement methods after the test runs and we no longer want to interact with the driver

			driver.closeApp();
			driver.quit();

		}

	}
